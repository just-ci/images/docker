# 1.0.0 (2022-06-06)


### Features

* add curl and yq ([867190c](https://gitlab.com/just-ci/images/docker/commit/867190ca752a9c11bf754e82dd4d038aa7c9de00))
