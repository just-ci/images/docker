# Docker V2

`docker:git` container with Docker V2, `curl` and `yq`.

```yaml
image: registry.gitlab.com/just-ci/images/docker:latest
```
