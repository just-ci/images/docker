FROM docker:git

RUN apk add --no-cache curl yq

RUN mkdir -p ~/.docker/cli-plugins/

RUN wget https://github.com/docker/compose/releases/latest/download/docker-compose-linux-x86_64 -O ~/.docker/cli-plugins/docker-compose && \
chmod +x ~/.docker/cli-plugins/docker-compose
